# Paraview-related libs
import paraview
from paraview.simple import *
from paraview import catalyst

"""
Sample Paraview-Python script 
"""

paraview.compatibility.major = 5
paraview.compatibility.minor = 11

print("Begin Paraview Python script")
print(f"Arguments provided by Catalyst: {paraview.catalyst.get_args()}")

frame = PVTrivialProducer(registrationName='frame')

paraview.simple._DisableFirstRenderCameraReset()

# Create a view
renderView = CreateView('RenderView')
renderView.ViewSize = [500, 500]
renderView.InteractionMode = '2D'
renderView.AxesGrid = 'GridAxes3DActor'
renderView.OrientationAxesVisibility = 0
renderView.CenterOfRotation = [0.5, 0.5, 0.0]
renderView.StereoType = 'Crystal Eyes'
renderView.CameraPosition = [0.5, 0.5, 3]
renderView.CameraFocalPoint = [0.5, 0.5, 0.0]
renderView.CameraFocalDisk = 1.0
renderView.CameraParallelScale = 0.5
renderView.UseColorPaletteForBackground = 1

renderView.ResetCamera()

# Create a layout
layout = CreateLayout(name=f'Layout')
layout.AssignView(0, renderView)
layout.SetSize(500, 500)

# Show the dataset received through Catalyst
display = Show(frame, renderView, 'GeometryRepresentation')
display.Representation = 'Surface With Edges'


# Save the render to PNG
PNG_Extractor = CreateExtractor('PNG', renderView, registrationName='Extractor')
PNG_Extractor.Trigger = 'TimeStep'
PNG_Extractor.Writer.ImageResolution = [500, 500]
PNG_Extractor.Writer.Format = 'PNG'
SetActiveSource(PNG_Extractor)

# Set Catalyst options
options = catalyst.Options()
options.ExtractsOutputDirectory = 'output'
options.GlobalTrigger = 'TimeStep'
options.EnableCatalystLive = 1
options.CatalystLiveTrigger = 'TimeStep'

# Function executed each frame
def catalyst_execute(info):
    print(f"Python Executing cycle {info.cycle}")
