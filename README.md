# Catalyst Player

![Logo Catalyst player](logo.svg)

Play XML VTK dataset timeseries files from disk to Catalyst to emulate a simulation.

Catalyst-player currently supports `.vtu`, `.vtr` and `.vti` file formats. All point and cell fields using Conduit-compatible types (`char`, `short`, `int`, `unsigned int`, `long`, `float`, `double`) are serialized and sent. `vtu` files do not support mixed types of cells.

To build the project, set CMake cache variables `VTK_DIR` and `catalyst_DIR`.

## Usage

```bash
catalyst_player [-v LOGGING_LEVEL] FILENAME_FORMAT PYTHON_SCRIPT [TIMESTEP_START] [TIMESTEP_STOP] [SLEEP_MS] [ARGS]
```

where

- `LOGGING_LEVEL` is the desired verbosity level. Use `OFF` to disable all output and `ERROR` to only show error messages. When not specified, all messages are displayed.
- `FILENAME_FORMAT` defines the input files format, including full path. Insert `#` characters where the timestep should be inserted. The number of `#` defines the padding size. For example, to play a timeseries located in `/tmp` using files names `dataset_01.vti`, `dataset_02.vti`, `dataset_03.vti`... `FILENAME_FORMAT` should be `/tmp/dataset_##.vti`.
- `PYTHON_SCRIPT` is the path to the Python script used by Paraview-Catalyst.
- `TIMESTEP_START` (optional) specifies the first timestep to be played. Defaults to 0.
- `TIMESTEP_STOP` (optional) specifies the last timestep to be played. Defaults to the timestep of the last file that can be found.
- `SLEEP_MS` (optional) is the time to wait before sending the next step. Defaults to 0.
- `ARGS` (optional) Arguments to pass to the Python script through Catalyst, separated by commas.

Don't forget to set environment variables `CATALYST_IMPLEMENTATION_NAME` to `paraview` and `CATALYST_IMPLEMENTATION_PATHS` to the Paraview Catalyst implementation before launching the script if used with Paraview.

Sample datasets are provided in `example_data` for `.vtr`, `.vts` and `.vtu` formats.

## Test

Build with `BUILD_TESTING` active, then run `ctest`. You can use either the stub implementation or the Paraview Catalyst implementation for testing, but Paraview may trigger warnings because we run `Initialize` and `Finalize` multiple times.

## Examples

The `examples/` directory contains a sample script and small datasets of different kinds.

When the project if built in a `build` directory, you can run the examples using commands such as :
```bash
./catalyst_player ../examples/data/imagedata_#.vti  ../examples/script.py 0 1 5000 AB,C,D
./catalyst_player ../examples/data/recti_#.vtr  ../examples/script.py
./catalyst_player ../examples/data/unstruct_#.vtu  ../examples/script.py 2
```

## License

This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.
