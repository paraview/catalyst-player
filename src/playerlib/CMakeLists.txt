set(VTKlibs
  VTK::CommonDataModel
  VTK::IOCore
  VTK::IOImage
  VTK::IOLegacy
  VTK::IOXML
  VTK::IOCatalystConduit
)

add_library(playerlib STATIC playerlib.cxx playerlib.h catalyst_adaptor.h)
target_link_libraries(playerlib
  PRIVATE
  catalyst::catalyst
  ${VTKlibs}
)
