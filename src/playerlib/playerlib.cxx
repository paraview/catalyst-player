#include "catalyst_adaptor.h"

#include <filesystem>
#include <iostream>
#include <limits>
#include <thread>
#include <vector>

#include <vtkDataSet.h>
#include <vtkLogger.h>
#include <vtkNew.h>
#include <vtkXMLImageDataReader.h>
#include <vtkXMLRectilinearGridReader.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtksys/SystemTools.hxx>

namespace
{
template <class TReader>
vtkSmartPointer<vtkDataSet> ReadVtkXMLFile(std::string fileName)
{
  vtkNew<TReader> reader;
  reader->SetFileName(fileName.c_str());
  reader->Update();

  vtkSmartPointer<vtkDataSet> dataset;
  dataset.TakeReference(vtkDataSet::SafeDownCast(reader->GetOutput()->NewInstance()));
  dataset->ShallowCopy(reader->GetOutput());

  return dataset;
}
}

namespace CatalystPlayer
{
constexpr bool SUCCESS = true;
constexpr bool FAILURE = false;

bool Play(const std::vector<std::string>& args)
{
  int argc = args.size();
  // Parse arguments
  if (argc < 2 || argc > 8)
  {
    std::cerr
      << "Wrong number of arguments. Usage : catalyst_player [-v LOGGING_LEVEL] FILENAME_FORMAT "
         "PYTHON_SCRIPT [TIMESTEP_START] [TIMESTEP_STOP] [SLEEP_MS] [ARGS]"
      << std::endl;
    return FAILURE;
  }

  // Initialize logging
  vtkLogger::Init();
  vtkLogger::SetStderrVerbosity(vtkLogger::VERBOSITY_INFO);

  int offset = 0;
  if (args[0] == "-v")
  {
    offset = 2; // Skip "-v" & "LOGGING_LEVEL"

    if (args[1] == "ERROR")
    {
      vtkLogger::SetStderrVerbosity(vtkLogger::VERBOSITY_ERROR);
    }
    else if (args[1] == "OFF")
    {
      vtkLogger::SetStderrVerbosity(vtkLogger::VERBOSITY_OFF);
    }
  }

  // Mandatory arguments
  std::string filenameFormat = args[offset + 0];
  std::string scriptPath = args[offset + 1];

  // Parse filename
  std::string prefix, suffix;
  unsigned short padding = 0;
  for (int i = 0; i < filenameFormat.size(); i++)
  {
    if (filenameFormat[i] == '#')
    {
      padding++;
    }
    else if (padding == 0)
    {
      prefix += filenameFormat[i];
    }
    else
    {
      suffix += filenameFormat[i];
    }
  }

  // Optional arguments
  unsigned int timestepStart = 0;
  unsigned int timestepStop = std::numeric_limits<unsigned int>::max();
  unsigned int sleepMs = 0;
  if (argc > 2 + offset)
  {
    timestepStart = std::stoi(args[offset + 2]);
    if (argc > 3 + offset)
    {
      timestepStop = std::stoi(args[offset + 3]);
      if (argc > 4 + offset)
      {
        sleepMs = std::stoi(args[offset + 4]);
      }
    }
  }

  // Initialize Catalyst, passing arguments to the script if provided
  if (argc == offset + 6)
  {
    Adaptor::Initialize(scriptPath.c_str(), args[offset + 5]);
  }
  else
  {
    Adaptor::Initialize(scriptPath.c_str());
  }

  for (unsigned int timestep = timestepStart; timestep <= timestepStop; timestep++)
  {
    std::stringstream ss_filename;
    ss_filename << prefix << std::setw(padding) << std::setfill('0') << timestep << suffix;
    std::string filename = ss_filename.str();

    if (!std::filesystem::exists(filename))
    {
      if (timestep == timestepStart)
      {
        vtkErrorWithObjectMacro(nullptr, << "File " << filename << " not found, exiting");
        return FAILURE;
      }
      vtkDebugWithObjectMacro(
        nullptr, "Timestep " << timestep << " not found, finalizing simulation");
      break;
    }

    vtkDebugWithObjectMacro(nullptr, << "Loading " << filename);

    vtkSmartPointer<vtkDataSet> dataset;
    std::string extension = vtksys::SystemTools::GetFilenameLastExtension(filename);

    if (extension == ".vti")
    {
      dataset = ReadVtkXMLFile<vtkXMLImageDataReader>(filename);
    }
    else if (extension == ".vtr")
    {
      dataset = ReadVtkXMLFile<vtkXMLRectilinearGridReader>(filename);
    }
    else if (extension == ".vtu")
    {
      dataset = ReadVtkXMLFile<vtkXMLUnstructuredGridReader>(filename);
    }
    else
    {
      vtkErrorWithObjectMacro(nullptr, << args[0] << " Unknown extension: " << extension);
      return FAILURE;
    }

    bool result = Adaptor::SendTimestep(dataset, 60.0 * timestep, timestep);
    if (!result)
    {
      vtkErrorWithObjectMacro(nullptr, "Error while serializing dataset");
      return FAILURE;
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(sleepMs));
  }

  Adaptor::Finalize();

  return SUCCESS;
}
} // namespace CatalystPlayer
