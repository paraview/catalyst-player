#include <string>
#include <vector>

namespace CatalystPlayer {
int Play(const std::vector<std::string> &args);
} // namespace CatalystPlayer
