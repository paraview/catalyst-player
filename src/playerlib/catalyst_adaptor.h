#include <catalyst.hpp>

#include <vtkDataObjectToConduit.h>
#include <vtkImageData.h>
#include <vtkRectilinearGrid.h>
#include <vtkUnstructuredGrid.h>

namespace Adaptor
{

bool IsCatalystErrorCodeSuccess(catalyst_status errorCode)
{
  if (errorCode != catalyst_status_ok)
  {
    vtkErrorWithObjectMacro(nullptr, << "Catalyst API call failed with error code: " << errorCode);
  }

  return errorCode == catalyst_status_ok;
}

bool Initialize(const char* scriptPath, const std::string& args = "")
{
  vtkDebugWithObjectMacro(nullptr,"Initialize Catalyst");

  conduit_cpp::Node node;
  auto script = node["catalyst/scripts/script"];
  script["filename"].set_string(scriptPath);

  // Parse arguments
  std::stringstream ss(args);
  while (ss.good())
  {
    std::string stringArg;
    getline(ss, stringArg, ',');

    conduit_cpp::Node conduitArg = script["args"].append();
    conduitArg.set_string(stringArg.c_str());
  }

  auto errorCode = catalyst_initialize(conduit_cpp::c_node(&node));
  return IsCatalystErrorCodeSuccess(errorCode);
}

bool SendTimestep(vtkDataSet* data, double time, unsigned int timeStep)
{
  vtkDebugWithObjectMacro(nullptr, "Sending timestep " << timeStep);
  vtkDebugWithObjectMacro(nullptr, "Type : " << data->GetClassName());

  vtkNew<vtkIdTypeArray> sizes; // Needed in this scope for VTU

  conduit_cpp::Node execParams;

  auto state = execParams["catalyst/state"];
  state["timestep"].set(timeStep);
  state["time"].set(time);

  auto channel = execParams["catalyst/channels/frame"];
  channel["type"].set("mesh");

  auto mesh = channel["data"];

  bool result;
  if (std::string(data->GetClassName()) == "vtkImageData")
  {
    vtkImageData* imageData = vtkImageData::SafeDownCast(data);
    result = vtkDataObjectToConduit::FillConduitNode(imageData, mesh);
  }
  else if (std::string(data->GetClassName()) == "vtkRectilinearGrid")
  {
    vtkRectilinearGrid* rectilinearGrid = vtkRectilinearGrid::SafeDownCast(data);
    result = vtkDataObjectToConduit::FillConduitNode(rectilinearGrid, mesh);
  }
  else if (std::string(data->GetClassName()) == "vtkUnstructuredGrid")
  {
    vtkUnstructuredGrid* unstructuredGrid = vtkUnstructuredGrid::SafeDownCast(data);
    result = vtkDataObjectToConduit::FillConduitNode(unstructuredGrid, mesh);
  }
  else
  {
    vtkErrorWithObjectMacro(nullptr, "Format not supported.");
    return false;
  }

  if (!result)
  {
    return false;
  }

  auto errorCode = catalyst_execute(conduit_cpp::c_node(&execParams));

  return IsCatalystErrorCodeSuccess(errorCode);
}

bool Finalize()
{
  vtkDebugWithObjectMacro(nullptr, "Finalize Catalyst");

  conduit_cpp::Node node;
  auto errorCode = catalyst_finalize(conduit_cpp::c_node(&node));

  return IsCatalystErrorCodeSuccess(errorCode);
}
} // namespace Adaptor
