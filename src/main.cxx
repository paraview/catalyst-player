#include "playerlib/playerlib.h"
#include <iostream>

#include <vtkObject.h>

int main(int argc, char* argv[])
{
  std::vector<std::string> args(argv, argv + argc);

  if (!CatalystPlayer::Play(std::vector<std::string>(args.begin() + 1, args.end())))
  {
    vtkErrorWithObjectMacro(nullptr,"Replay failed.");
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
