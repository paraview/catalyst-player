#include "playerlib/playerlib.h"
#include <iostream>
#include <vtkLogger.h>

using ArgsType = std::vector<std::string>;

bool testPlayer(ArgsType args) {
  return CatalystPlayer::Play(args);
}

bool testPlayerFails(ArgsType args) {
  return !CatalystPlayer::Play(args);
}

int main(int argc, char **argv) {
  bool result = true;

  #ifdef EXAMPLES_FOLDER
  std::string folder = std::string(EXAMPLES_FOLDER);

  result &= testPlayerFails(ArgsType{folder + "/data/unstruct_#.vtu"});
  result &= testPlayerFails(ArgsType{folder + "/data/not_existing_#.vtu", folder + "/script.py"});
  result &= testPlayerFails(ArgsType{folder + "/data/unstruct_#.vtu", folder + "/script.py", "3"});

  result &= testPlayer(ArgsType{folder + "/data/unstruct_#.vtu", folder + "/script.py"});
  result &= testPlayer(ArgsType{folder + "/data/unstruct_#.vtu", folder + "/script.py", "1"});
  result &= testPlayer(ArgsType{folder + "/data/unstruct_#.vtu", folder + "/script.py", "1", "2"});
  result &= testPlayer(ArgsType{folder + "/data/unstruct_#.vtu", folder + "/script.py", "1", "2", "50"});
  result &= testPlayer(ArgsType{folder + "/data/unstruct_#.vtu", folder + "/script.py", "1", "2", "50", "A,B,C"});
  
  result &= testPlayer(ArgsType{folder + "/data/imagedata_#.vti", folder + "/script.py"});
  result &= testPlayer(ArgsType{folder + "/data/recti_#.vtr", folder + "/script.py"});

  result &= testPlayer(ArgsType{"-v", "ERROR", folder + "/data/unstruct_#.vtu", folder + "/script.py", "1", "2", "50", "A,B,C"});
  result &= vtkLogger::GetCurrentVerbosityCutoff() == vtkLogger::VERBOSITY_ERROR;

  result &= testPlayer(ArgsType{"-v", "OFF", folder + "/data/unstruct_#.vtu", folder + "/script.py", "1", "2", "50", "A,B,C"});
  result &= vtkLogger::GetCurrentVerbosityCutoff() == vtkLogger::VERBOSITY_OFF;

  result &= testPlayer(ArgsType{"-v", "OTHER", folder + "/data/unstruct_#.vtu", folder + "/script.py", "1", "2", "50", "A,B,C"});
  result &= vtkLogger::GetCurrentVerbosityCutoff() == vtkLogger::VERBOSITY_INFO;

  return !result;

  #else
  return EXIT_FAILURE;
  #endif
}
